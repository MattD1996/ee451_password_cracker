# To compile the serial code:

Make sure you have **openssl** installed and compile with the **-lssl** and **-lcrypto** flags at the end of your compile. For example,
```
gcc -g -o cracker cracker.c counter.h -lssl -lcrypto
```

# To compile the parallel code:
## Installation Instructions

* [**Intel Tbb Library**](https://github.com/01org/tbb/blob/master/download.md) V4.3 Update 6 is available in the repo as the *tbb* folder. 

* Add the path to the *tbb* folder to LD_LIBRARY_PATH variable as follows. *cd* into the folder containing the tbb folder and execute the following in the terminal.
```
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:./tbb/lib/"
```

* Run make file

* Run program. *passwords.txt* contains sha1 hashes that are going to be cracked.

```
./program passwords.txt
```
