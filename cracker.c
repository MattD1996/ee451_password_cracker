#include "counter.h"
#include <openssl/sha.h>
#include "string.h"

void serialCracker(char* startVal, char* endVal, unsigned char* targetHash)
{
    struct Password* password = new_password(6);
    SetPassword(startVal, 6, password);
    printf("lol Pass is: %s\n", GetPassword(password));
    unsigned char hash[SHA512_DIGEST_LENGTH];
    char* passOutput = (char*) malloc(password->numOfBlocks + 1* sizeof(char));
    passOutput = GetPassword(password);
    SHA512(passOutput, strlen(passOutput), hash);
    printf("Hash: ");
    int i;
    for (i = 0; i < SHA512_DIGEST_LENGTH; i++)
    {
        printf("%02X", hash[i]);
    }
    printf("\n");
    printf("TargetHash: ");
    for (i = 0; i < SHA512_DIGEST_LENGTH; i++)
    {
        printf("%02X", targetHash[i]);
    }
    printf("\n");
    while(memcmp(targetHash, hash, sizeof(hash)) != 0 && passOutput != endVal)
    {
        IncrementPassword(password);
        passOutput = GetPassword(password);
        SHA512(passOutput, strlen(passOutput), hash);
    }
    if (memcmp(targetHash, hash, sizeof(hash)) == 0)
    {
        printf("Password has been found: %s\n", passOutput);
    }
    delete_password(password);
}

int main(int argc, char** argv)
{
    struct Password* password = new_password(6);
    //  int i;
    //  for(i = 0; i< 4000; i++)
    //  {
    //      PrintPassword(password);
    //      IncrementPassword(password);
    //  }
     printf("Hashing 000005: \n");
     char* newPass = "000005";
     SetPassword(newPass, 6, password);
     //Running a quick check
     printf("Pass is: %s\n", GetPassword(password));
    //  if(newPass == GetPassword(password))
    //  {
    //      printf("All systems are go for your assumption\n");
    //  }
    //  else
    //  {
    //      printf("Check the null character jackass\n");
    //  }
     //PrintPassword(password);
     char* passOutput = (char*) malloc(password->numOfBlocks + 1* sizeof(char));
     passOutput = GetPassword(password);
     //printf("Our password output from GetPassword is: %s\n", passOutput);
     //trying to hash the password
     unsigned char hash[SHA512_DIGEST_LENGTH];
     SHA512(passOutput, strlen(passOutput), hash);
     serialCracker("000000", "aaaaaa", hash);
    //  int i;
    //  for (i = 0; i < SHA512_DIGEST_LENGTH; i++)
    //  {
    //      printf("%02X", hash[i]);
    //  }
    //  printf("\n");
    delete_password(password);
}