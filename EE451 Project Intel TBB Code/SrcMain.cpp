#include "SrcMain.h"

#include <algorithm>
#include <fstream>

#include "BruteForceCracking.h"

int main(int argc, const char* argv[])
{
    ProcessCommandArgs(argc, argv);
    return 0;
}
void ProcessCommandArgs(int argc, const char* argv[])
{
    if (argc == 2)
    {
        // Files to load data from.
        const char *passwordFileName = argv[1];
        
        // Stores all hashes for which passwords need to be looked up.
        std::vector<std::string> allHashes;
        
        // Get all hashes.
        std::ifstream pwdFile(passwordFileName);
        if (pwdFile.is_open())
        {
            std::string line;
            while (static_cast<bool>(std::getline(pwdFile, line)))
            {
                allHashes.emplace_back(line);
            }
            pwdFile.close();
        }
        
        // Brute force cracking.
        auto brute = BruteForceCracking(allHashes);
        
        // Output the answer to solved.txt
        std::ofstream output("solved.txt");
        std::string password;

        // Loop through all hashes and look up their password either using dictionary or brute force.
        std::for_each(allHashes.begin(),
                      allHashes.end(),
                      [&brute, &password, &output] (std::string &hash) {
                          
                          bool success = brute.GetPassword(hash, password);
                          if (success)
                          {
                              output << hash << "," << password << std::endl;
                          }
                          else
                          {
                              output << hash << "," << "??" << std::endl;
                          }
                      });
        
        // Close the output file.
        output.close();
    }
}
