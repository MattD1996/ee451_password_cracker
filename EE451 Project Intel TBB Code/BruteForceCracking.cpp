#include "BruteForceCracking.h"

#include <algorithm>
#include <cmath>
#include <cstring>
#include <tbb/parallel_invoke.h>

#include "sha1.h"

namespace math
{
    // Increment the integers in an array with base 36.
    void Increment(std::vector<int> &value, int base)
    {
        // Go back from back of array to front.
        int len = value.size() - 1;
        int carryout = 1;
        while (len >= 0 && carryout > 0)
        {
            int sum = value[len] + carryout;
            carryout = sum / base;
            value[len] = sum % base;
            --len;
        }
    }
}

static char IntToChar[] =
{
	'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j',
    'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y',
    'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8',
    '9'
};

// Base 36
static const int Base = 36;

bool BruteForceCracking::GetPassword(std::string &hash, std::string &password)
{
    if (mEncryptionToPassword.find(hash) != mEncryptionToPassword.end())
    {
        password = mEncryptionToPassword[hash];
        return true;
    }
    return false;
}

BruteForceCracking::BruteForceCracking(std::vector<std::string> &hashes)
{
    // Length of passwords vary from 1 to 4.
    const static int kLength = 4;
    
    // Parition the data into 9 partitions.
    const static int kRange = 9;
    
    // Contains the partition numbering.
    std::vector<std::vector<int>> bruteForce(kRange);
    
    // Loop from length 1 to length 4.
    for (int j = 0; j < kLength; ++j)
    {
        // Split into 9 ranges.
        int index = 0;
        int length = j + 1;
        for (int i = 0; i < kRange; ++i)
        {
            // Initialize
            bruteForce[i].resize(length);
            // Data is writter as
            // [0] = 0000
            // [1] = 400 and so on.
            // The ints are then looked up in IntToChar to get the brute force password.
            bruteForce[i][0] = index;
            for (int j = 1; j < length; ++j)
            {
                bruteForce[i][j] = 0;
            }
            index += 4;
        }
        
        // How many elements each partition.
        int count = 4 * pow(36, (length - 1));
        tbb::parallel_invoke(
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[0], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[1], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[2], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[3], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[4], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[5], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[6], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[7], count);
                             },
                             [this, &hashes, count, &bruteForce] {
                                 BruteForceHelper(hashes, bruteForce[8], count);
                             }
                             );
    }
}

void BruteForceCracking::BruteForceHelper(std::vector<std::string> &hashes,
                                          std::vector<int> &value,
                                          int count)
{
    // Select string to hash.
    int arrayLen = value.size();
    char *input = new char[arrayLen + 1];
    
    // Calculate hash.
    for (int i = 0; i < count; ++i)
    {
        for (int j = 0; j < arrayLen; ++j)
        {
            input[j] = IntToChar[value[j]];
        }
        input[arrayLen] = '\0';

        // Hash calculation
        unsigned char hash[20];
        sha1::Calc(input, strlen(input), hash);
        char hexStr[41];
        sha1::ToHexString(hash, hexStr);

        // If hash matches then store it.
        auto itr = std::find(hashes.begin(), hashes.end(), hexStr);
        if (itr != hashes.end())
        {
            mEncryptionToPassword[hexStr] = input;
        }

        // Increment the integer.
        math::Increment(value, Base);
    }
	
    // Delete input allocation.
    delete [] input;
}
