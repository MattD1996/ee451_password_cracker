#pragma once

#include <string>
#include <unordered_map>
#include <vector>

namespace math
{
    // Increment the integers in an array with the base provided.
    void Increment(std::vector<int> &value, int base);
}

class BruteForceCracking
{
public:
    // Check brute force against hashes passed in the vector.
    BruteForceCracking(std::vector<std::string> &hashes);
    
    // Check password for the given hash.
    bool GetPassword(std::string& hash, std::string& password);
    
private:
    // Helper function called by tbb::parallel_invoke
    void BruteForceHelper(std::vector<std::string> &hashes,
                          std::vector<int> &value,
                          int count);
    
    // Mapping hashes to passwords.
    std::unordered_map<std::string, std::string> mEncryptionToPassword;
};
