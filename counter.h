#ifndef COUNTER_H
#define COUNTER_H
#include <stdlib.h>
#include <stdio.h>  

//a character block that will hold the values [0-9a-zA-Z]
struct Block
{
    //the value held by the block
    int val;
    //used when the value maxes out to signal a need to propogate
    int propogate;
};

struct Password
{
    //The zeroith element corresponds to the rightmost character
    int numOfBlocks;
    struct Block** blocks;
};

struct Password* new_password(int passwordSize)
{
    struct Password* password = malloc(sizeof(struct Password));
    password->blocks = malloc(sizeof(struct Block*) * passwordSize);
    password->numOfBlocks = passwordSize;
    int i;
    for(i = 0; i < password->numOfBlocks; i++)
    {
        password->blocks[i] = malloc(sizeof(struct Block));
        password->blocks[i]->val = 0;
        password->blocks[i]->propogate = 0;
    }
    return password;
}

void delete_password(struct Password* password)
{
    int i;
    for(i = 0; i < password->numOfBlocks; i++)
    {
        free(password->blocks[i]);
    }
    free(password->blocks);
    free(password);
}

void SetBlock(struct Block* block, int val)
{
   //0->9 = [0-9]
   //a->z = [10->35]
   //A->Z = [36-61]
   if(val >= 0 && val < 62)
   {
        block->val = val;
        block->propogate = 0;
   }
}

void IncrementBlock(struct Block* block)
{
    if(block->val < 61)
    {
        block->val += 1;
        if(block->val == 61)
        {
            block->propogate = 1;
        }
    }
}

int canPropogate(struct Password* password, int blockIndex)
{
    if(password->blocks[blockIndex]->propogate == 1)
    {
        if(blockIndex != password->numOfBlocks - 1) 
        {
            canPropogate(password, blockIndex + 1);
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 1;
    }
}

void Increment(struct Password* password, int blockIndex)
{
   if(blockIndex < password->numOfBlocks)
   {
      //check to see if the propogate block is set
      if(password->blocks[blockIndex]->propogate == 1)
      {
          //check to see if you can propogate
          if(canPropogate(password, blockIndex))
          {
              SetBlock(password->blocks[blockIndex], 0);
              Increment(password, blockIndex + 1);
          }
      }
      else
      {
          IncrementBlock(password->blocks[blockIndex]);
      }
   }
}

void IncrementPassword(struct Password* password)
{
    Increment(password, 0);
}

void PrintPassword(struct Password* password)
{
    int i;
    for(i = (password->numOfBlocks - 1); i >= 0; i--)
    {
        if(password->blocks[i]->val <= 9)
        {
            printf("%d ", password->blocks[i]->val);
        }
        else if (password->blocks[i]->val <= 35)
        {
            printf("%c ", password->blocks[i]->val + 87);
        }
        else if (password->blocks[i]->val < 62)
        {
            printf("%c ", password->blocks[i]->val + 29);
        }
    }
    printf("\n");
}

//extract block value from char
int GetBlockValFromChar(char c)
{
    if(c >= '0' && c <= 'z')
    {
        if(c <= '9')
        {
            return c-48;
        }
        else if (c <= 'Z')
        {
            return c-29;
        }
        else if (c <= 'z')
        {
            return c-87;
        }
    }
    return 0;
}
//set password
void SetPassword(char *newPass, int size, struct Password* password)
{
   int i;
   for(int i = 0; i<size; i++)
   {
       int blockVal = GetBlockValFromChar(newPass[i]);
       SetBlock(password->blocks[size-1-i], blockVal);
   }
}

char* GetPassword(struct Password* password)
{
    char* passstring = (char*) malloc(password->numOfBlocks + 1 * sizeof(char));
    int i;
    for(i = (password->numOfBlocks - 1); i >= 0; i--)
    {
        if(password->blocks[i]->val <= 9)
        {
             passstring[password->numOfBlocks - 1 - i] = password->blocks[i]->val + 48;
        }
        else if (password->blocks[i]->val <= 35)
        {
             passstring[password->numOfBlocks - 1 - i] = password->blocks[i]->val + 87;
        }
        else if (password->blocks[i]->val < 62)
        {
            passstring[password->numOfBlocks - 1 - i] = password->blocks[i]->val + 29;
        }
        //printf("Passtring[%i] = %c\n", password->numOfBlocks - 1 - i, passstring[password->numOfBlocks - 1 - i]);
    }
    passstring[password->numOfBlocks] = '\0';
    return passstring;
}
#endif
